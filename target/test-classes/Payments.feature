Feature: Verify Payments functionality

  @VerifyPaymentSubmitprocess
  Scenario Outline: Performing a payment Transaction using Payments
    Given Launch Payment center application
   When I set userName to "itqa@sage.com"
   And I set password to "Sage1234"
   And I click on Sign In
   And I click on continue
    Given In payment center Portal click on Accept Payments option.
    And I set cardNumber to <CardNumber>
    And I set subtotal to <Subtotal>
    And I click on HideIcon
    And I set firstName to <FirstName>
    And I set lastName to <LastName>
    When I click on Submit
    Then response message should be 'Approved'

    Examples: 
      | CardName  | CardNumber       | ExpirationMonth | ExpirationYearIndex | CVV |FirstName|LastName|Subtotal|
      | Visa      | 4111111111111111 |               4 |                   2 | 123 |test     |test    |50      |
      |	Mastercard|	5454545454545454 |			 				 6 |                   5 | 123 |					
		  |	Discover	|	6011000993026909 |               7 |                   6 | 123 |
	    |	Amex		  |	371449635392376  |               12|                   7 | 1234|