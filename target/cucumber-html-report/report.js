$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Login.feature");
formatter.feature({
  "line": 1,
  "name": "Verify the login functionality",
  "description": "",
  "id": "verify-the-login-functionality",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Successful Login with Valid Credentials",
  "description": "",
  "id": "verify-the-login-functionality;successful-login-with-valid-credentials",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@VerifyCreditCardPayment"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Launch Payment center application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I set userName to itqa@sage.com",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I set password to Sage1234",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I click on Sign In",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on continue",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "In payment center Portal click on Accept Payments option.",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I choose credit card option",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I set cardNumber to \u003cCardNumber\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I set subtotal to \u003cSubtotal\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I click on HideIcon",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I set firstName to \u003cFirstName\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I set lastName to \u003cLastName\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I click on Submit",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "response message should be \u0027Payment Submitted\u0027",
  "keyword": "Then "
});
formatter.examples({
  "line": 20,
  "name": "",
  "description": "",
  "id": "verify-the-login-functionality;successful-login-with-valid-credentials;",
  "rows": [
    {
      "cells": [
        "name",
        "password",
        "CardName",
        "CardNumber",
        "ExpirationMonth",
        "ExpirationYearIndex",
        "CVV",
        "FirstName",
        "LastName",
        "Subtotal"
      ],
      "line": 21,
      "id": "verify-the-login-functionality;successful-login-with-valid-credentials;;1"
    },
    {
      "cells": [
        "itqa@sage.com",
        "Sage1234",
        "Visa",
        "4111111111111111",
        "4",
        "2",
        "123",
        "test",
        "test",
        "70"
      ],
      "line": 22,
      "id": "verify-the-login-functionality;successful-login-with-valid-credentials;;2"
    },
    {
      "cells": [
        "itqa@sage.com",
        "Sage1234",
        "Mastercard",
        "5499740000000057",
        "4",
        "2",
        "123",
        "test",
        "test",
        "50"
      ],
      "line": 23,
      "id": "verify-the-login-functionality;successful-login-with-valid-credentials;;3"
    },
    {
      "cells": [
        "itqa@sage.com",
        "Sage1234",
        "Discover",
        "6011000993026909",
        "4",
        "2",
        "123",
        "test",
        "test",
        "30"
      ],
      "line": 24,
      "id": "verify-the-login-functionality;successful-login-with-valid-credentials;;4"
    },
    {
      "cells": [
        "itqa@sage.com",
        "Sage1234",
        "Amex",
        "37144963539237",
        "4",
        "2",
        "123",
        "test",
        "test",
        "50"
      ],
      "line": 25,
      "id": "verify-the-login-functionality;successful-login-with-valid-credentials;;5"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 23972735200,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Successful Login with Valid Credentials",
  "description": "",
  "id": "verify-the-login-functionality;successful-login-with-valid-credentials;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@VerifyCreditCardPayment"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Launch Payment center application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I set userName to itqa@sage.com",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I set password to Sage1234",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I click on Sign In",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on continue",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "In payment center Portal click on Accept Payments option.",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I choose credit card option",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I set cardNumber to 4111111111111111",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I set subtotal to 70",
  "matchedColumns": [
    9
  ],
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I click on HideIcon",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I set firstName to test",
  "matchedColumns": [
    7
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I set lastName to test",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I click on Submit",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "response message should be \u0027Payment Submitted\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.PaymentCenter_portal_opened()"
});
formatter.result({
  "duration": 6553043600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "itqa@sage.com",
      "offset": 18
    }
  ],
  "location": "Login.set_userName(String)"
});
formatter.result({
  "duration": 3537661500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sage1234",
      "offset": 18
    }
  ],
  "location": "Login.set_password(String)"
});
formatter.result({
  "duration": 3372829700,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_SignIn()"
});
formatter.result({
  "duration": 9256894900,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_continue()"
});
formatter.result({
  "duration": 44430016500,
  "status": "passed"
});
formatter.match({
  "location": "Payments.Click_AcceptPayments()"
});
formatter.result({
  "duration": 38905110500,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_on_creditcardpayment_options()"
});
formatter.result({
  "duration": 1278871900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4111111111111111",
      "offset": 20
    }
  ],
  "location": "Payments.set_cardNumber(String)"
});
formatter.result({
  "duration": 2368158300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "70",
      "offset": 18
    }
  ],
  "location": "Payments.set_subTotal(String)"
});
formatter.result({
  "duration": 3439661100,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_HideIcon()"
});
formatter.result({
  "duration": 9362812000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 19
    }
  ],
  "location": "Payments.firstName(String)"
});
formatter.result({
  "duration": 3566354400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 18
    }
  ],
  "location": "Payments.lastName(String)"
});
formatter.result({
  "duration": 3541900600,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_Submit()"
});
formatter.result({
  "duration": 6314990300,
  "status": "passed"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.write("Test ended last");
formatter.after({
  "duration": 1776465700,
  "status": "passed"
});
formatter.before({
  "duration": 13870573300,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Successful Login with Valid Credentials",
  "description": "",
  "id": "verify-the-login-functionality;successful-login-with-valid-credentials;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@VerifyCreditCardPayment"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Launch Payment center application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I set userName to itqa@sage.com",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I set password to Sage1234",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I click on Sign In",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on continue",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "In payment center Portal click on Accept Payments option.",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I choose credit card option",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I set cardNumber to 5499740000000057",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I set subtotal to 50",
  "matchedColumns": [
    9
  ],
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I click on HideIcon",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I set firstName to test",
  "matchedColumns": [
    7
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I set lastName to test",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I click on Submit",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "response message should be \u0027Payment Submitted\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.PaymentCenter_portal_opened()"
});
formatter.result({
  "duration": 6012922500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "itqa@sage.com",
      "offset": 18
    }
  ],
  "location": "Login.set_userName(String)"
});
formatter.result({
  "duration": 3466680500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sage1234",
      "offset": 18
    }
  ],
  "location": "Login.set_password(String)"
});
formatter.result({
  "duration": 3348354600,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_SignIn()"
});
formatter.result({
  "duration": 9265689700,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_continue()"
});
formatter.result({
  "duration": 39396830100,
  "status": "passed"
});
formatter.match({
  "location": "Payments.Click_AcceptPayments()"
});
formatter.result({
  "duration": 38906812200,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_on_creditcardpayment_options()"
});
formatter.result({
  "duration": 1314051400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5499740000000057",
      "offset": 20
    }
  ],
  "location": "Payments.set_cardNumber(String)"
});
formatter.result({
  "duration": 2096915200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 18
    }
  ],
  "location": "Payments.set_subTotal(String)"
});
formatter.result({
  "duration": 3507963100,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_HideIcon()"
});
formatter.result({
  "duration": 9366671300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 19
    }
  ],
  "location": "Payments.firstName(String)"
});
formatter.result({
  "duration": 3259164700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 18
    }
  ],
  "location": "Payments.lastName(String)"
});
formatter.result({
  "duration": 3541148300,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_Submit()"
});
formatter.result({
  "duration": 6326011400,
  "status": "passed"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.write("Test ended last");
formatter.after({
  "duration": 1367981300,
  "status": "passed"
});
formatter.before({
  "duration": 13575918400,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Successful Login with Valid Credentials",
  "description": "",
  "id": "verify-the-login-functionality;successful-login-with-valid-credentials;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@VerifyCreditCardPayment"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Launch Payment center application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I set userName to itqa@sage.com",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I set password to Sage1234",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I click on Sign In",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on continue",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "In payment center Portal click on Accept Payments option.",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I choose credit card option",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I set cardNumber to 6011000993026909",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I set subtotal to 30",
  "matchedColumns": [
    9
  ],
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I click on HideIcon",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I set firstName to test",
  "matchedColumns": [
    7
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I set lastName to test",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I click on Submit",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "response message should be \u0027Payment Submitted\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.PaymentCenter_portal_opened()"
});
formatter.result({
  "duration": 6018571900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "itqa@sage.com",
      "offset": 18
    }
  ],
  "location": "Login.set_userName(String)"
});
formatter.result({
  "duration": 3470712100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sage1234",
      "offset": 18
    }
  ],
  "location": "Login.set_password(String)"
});
formatter.result({
  "duration": 3399234200,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_SignIn()"
});
formatter.result({
  "duration": 9167077000,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_continue()"
});
formatter.result({
  "duration": 39263794300,
  "status": "passed"
});
formatter.match({
  "location": "Payments.Click_AcceptPayments()"
});
formatter.result({
  "duration": 38818159500,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_on_creditcardpayment_options()"
});
formatter.result({
  "duration": 1202041800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "6011000993026909",
      "offset": 20
    }
  ],
  "location": "Payments.set_cardNumber(String)"
});
formatter.result({
  "duration": 1573287400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "30",
      "offset": 18
    }
  ],
  "location": "Payments.set_subTotal(String)"
});
formatter.result({
  "duration": 3243987200,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_HideIcon()"
});
formatter.result({
  "duration": 9406274600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 19
    }
  ],
  "location": "Payments.firstName(String)"
});
formatter.result({
  "duration": 3554001200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 18
    }
  ],
  "location": "Payments.lastName(String)"
});
formatter.result({
  "duration": 3547016300,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_Submit()"
});
formatter.result({
  "duration": 6305670600,
  "status": "passed"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.write("Test ended last");
formatter.after({
  "duration": 1522793400,
  "status": "passed"
});
formatter.before({
  "duration": 13456768900,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Successful Login with Valid Credentials",
  "description": "",
  "id": "verify-the-login-functionality;successful-login-with-valid-credentials;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@VerifyCreditCardPayment"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Launch Payment center application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I set userName to itqa@sage.com",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I set password to Sage1234",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I click on Sign In",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on continue",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "In payment center Portal click on Accept Payments option.",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I choose credit card option",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I set cardNumber to 37144963539237",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I set subtotal to 50",
  "matchedColumns": [
    9
  ],
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I click on HideIcon",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I set firstName to test",
  "matchedColumns": [
    7
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I set lastName to test",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I click on Submit",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "response message should be \u0027Payment Submitted\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.PaymentCenter_portal_opened()"
});
formatter.result({
  "duration": 6010352500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "itqa@sage.com",
      "offset": 18
    }
  ],
  "location": "Login.set_userName(String)"
});
formatter.result({
  "duration": 3471405800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sage1234",
      "offset": 18
    }
  ],
  "location": "Login.set_password(String)"
});
formatter.result({
  "duration": 3384207800,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_SignIn()"
});
formatter.result({
  "duration": 9272176000,
  "status": "passed"
});
formatter.match({
  "location": "Login.click_continue()"
});
formatter.result({
  "duration": 40164228900,
  "status": "passed"
});
formatter.match({
  "location": "Payments.Click_AcceptPayments()"
});
formatter.result({
  "duration": 38782878800,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_on_creditcardpayment_options()"
});
formatter.result({
  "duration": 1309924500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "37144963539237",
      "offset": 20
    }
  ],
  "location": "Payments.set_cardNumber(String)"
});
formatter.result({
  "duration": 2097041800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50",
      "offset": 18
    }
  ],
  "location": "Payments.set_subTotal(String)"
});
formatter.result({
  "duration": 3444452500,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_HideIcon()"
});
formatter.result({
  "duration": 9259090300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 19
    }
  ],
  "location": "Payments.firstName(String)"
});
formatter.result({
  "duration": 3486684500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 18
    }
  ],
  "location": "Payments.lastName(String)"
});
formatter.result({
  "duration": 3531548100,
  "status": "passed"
});
formatter.match({
  "location": "Payments.click_Submit()"
});
formatter.result({
  "duration": 6315299100,
  "status": "passed"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.write("Test ended last");
formatter.after({
  "duration": 1312617200,
  "status": "passed"
});
});