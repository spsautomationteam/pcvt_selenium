package com.paymentcenter.testcases;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import Utility.UtilityFunctions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login {

	public WebDriver driver;
	
	
	public Login()
	{
		driver= Hooks.driver;
	}
	
	@Given("^Launch Payment center application$")

	public void PaymentCenter_portal_opened() throws Throwable{
		UtilityFunctions.waitForPageToBeReady();
	}

	@When("^I set userName to (.*)$")
	public void set_userName(String userName) throws Throwable 
	{	UtilityFunctions.performAction("txtEmail",userName);
	//Enter card number'
	Thread.sleep(3000);	
	}
	@When("^I set password to (.*)$")
	public void set_password(String psw) throws Throwable 
	{	UtilityFunctions.performAction("txtPassword",psw);
	//Enter card number'
	Thread.sleep(3000);	
	}
	
	@When("^I select MID$")
	public void select_cusMID() throws Throwable 
	{	UtilityFunctions.performAction("strMerchantID","");
	//Enter card number'
	Thread.sleep(3000);	
	UtilityFunctions.waitForPageToBeReady();
	}
	
	@When("^I click on Sign In$")
	public void click_SignIn() throws Throwable 
	{	UtilityFunctions.performAction("btnSignIn","");
	//Enter card number'
	Thread.sleep(3000);	
	UtilityFunctions.waitForPageToBeReady();
	
	}
	
	@When("^I click on continue$")
	public void click_continue() throws Throwable 
	{	UtilityFunctions.performAction("btnContinue","");
	//Enter card number'
	Thread.sleep(3000);	
	UtilityFunctions.waitForPageToBeReady();
	Thread.sleep(30000);
	}
	
//	@Then("^Inline transaction response message should be 'Select a Merchant Account'$")
//	 public void Get_MIdMsg() throws Throwable {
//		Thread.sleep(3000);
//		UtilityFunctions.performAction("hdrMerchandId","");
//		String testdata=Hooks.driver.getTitle();
//		System.out.println("header text data:"+testdata);
//		//Assert.assertEquals("Select a Merchant Account",Hooks.driver.getTitle());
//		Thread.sleep(3000);	
//		
//	}
}
