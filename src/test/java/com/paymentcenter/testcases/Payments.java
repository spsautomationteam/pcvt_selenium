package com.paymentcenter.testcases;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Utility.UtilityFunctions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Payments {

	public WebDriver driver;
	
	public Payments()
	{
		driver=Hooks.driver;
	}
	
	@Given("^In payment center Portal click on Accept Payments option.$")
	public void Click_AcceptPayments() throws Throwable {
		
		UtilityFunctions.performAction("menuMerchantAccount","");
		Thread.sleep(10000);
		System.out.println("menuMerchantAccount");
		UtilityFunctions.performAction("menuPayments","");
		Thread.sleep(10000);
		System.out.println("menuPayments");
		UtilityFunctions.performAction("menuAcceptPymnt","");
		//Thread.sleep(30000);
		System.out.println("menuAcceptPymnt");
//		String VTPayment=driver.findElements(VTPaymentwindow);
		UtilityFunctions.switchBetweenTabs(1);
		Thread.sleep(1000);
		
	}
	
	@Given("^I set subtotal to (.*)$")
	public void set_subTotal(String subTotal) throws Throwable 
	{	
		//Enter card number'
		UtilityFunctions.performAction("txtSubtotal",subTotal);
		Thread.sleep(3000);
		
	}
	
	@When("^I choose virtualCheck option$")
	public void click_on_virtualpayment_options() throws Throwable
	{
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		System.out.println("test");
		UtilityFunctions.performAction("btnVcheckRadio", "");
	}
	
	@When("^I choose credit card option$")
	public void click_on_creditcardpayment_options() throws Throwable
	{
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		System.out.println("test1");
		UtilityFunctions.performAction("btnCcardRadio", "");
		Thread.sleep(1000);
	}
	
	
	@Given("^I set cardNumber to (.*)$")
	public void set_cardNumber(String cardNum) throws Throwable 
	{	
		//Enter card number'
		UtilityFunctions.performAction("txtCardNum",cardNum);
		Thread.sleep(1000);
		
	}
	
	@When("^I click on HideIcon$")
	public void click_HideIcon() throws Throwable 
	{	UtilityFunctions.performAction("hidecheckbox","");
	//Enter card number'
	Thread.sleep(3000);	
	UtilityFunctions.waitForPageToBeReady();
	}

	@Given("^I set firstName to (.*)$")
	public void firstName(String fstname) throws Throwable 
	{	
		//Enter first name
		UtilityFunctions.performAction("txtfstName",fstname);
		Thread.sleep(3000);
		
	}

	@Given("^I set lastName to (.*)$")
	public void lastName(String lastname) throws Throwable 
	{	
		//Enter last name
		UtilityFunctions.performAction("txtlastName",lastname);
		Thread.sleep(3000);
		
	}
	
	@Given("^I set routingNum to (.*)$")
	public void setRoutingNum(String routingNum) throws Throwable
	{
		//Enter routingNum
		UtilityFunctions.performAction("txtRoutingNum", routingNum);
		Thread.sleep(1000);
	}

	
	@Given("^I set accountnum to (.*)$")
	public void setAccountNum(String accountNum) throws Throwable
	{
		//Enter routingNum
		UtilityFunctions.performAction("txtAccountnum", accountNum);
		Thread.sleep(1000);
	}
	
	@Given("^I set address to (.*)$")
	public void setAddress(String address) throws Throwable 
	{	
		//Enter last name
		UtilityFunctions.performAction("txtAddress",address);
		Thread.sleep(1000);
	}
	
	@Given("^I set city to (.*)$")
	public void setCity(String city) throws Throwable 
	{	
		//Enter last name
		UtilityFunctions.performAction("txtCity",city);
		Thread.sleep(1000);
	}
	
	@Given("^I set zipcode to (.*)$")
	public void setzipcode(String zipcode) throws Throwable 
	{	
		//Enter last name
		UtilityFunctions.performAction("txtZIPCode",zipcode);
		Thread.sleep(1000);
	}
	
	@When("^I click on Submit$")
	public void click_Submit() throws Throwable 
		{	
		UtilityFunctions.scrollDownWindow();
		UtilityFunctions.performAction("btnSubmit","");	
	//Enter card number	
	UtilityFunctions.waitForPageToBeReady();
	}
	
		
	@Then("^response message should be 'Payment Submitted'.$")
	public void verify_response_message() throws Throwable 
	{
		//String str="";
		Thread.sleep(5000);
		WebElement successPopup = UtilityFunctions.retunWebElement("responsePopup") ;
		if(successPopup.isDisplayed())
		{
			UtilityFunctions.performValidation("responseSuccessMsg","Payment Submitted");
			
		}
   
	}
	
	
	
	
}
