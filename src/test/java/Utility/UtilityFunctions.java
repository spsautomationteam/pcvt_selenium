package Utility;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.paymentcenter.testcases.Hooks;

public class UtilityFunctions {
	
	public static String strTxType;
	public static String parentWindow;
		
	
	//*********************************************************************
	// function to create WebElement
	//*********************************************************************	
	public static WebElement retunWebElement(String strObj) throws IOException
	{
		Hooks.filereadConfig();
		WebElement element =null;
		String strObjProp =  Hooks.proprty.getProperty(strObj);
		String[] propParameters = strObjProp.split(",",2);
		String strKey = propParameters[0];
		String strValue = propParameters[1];
		
		switch(strKey.toLowerCase())
		{
			case "xpath":
				element = new WebDriverWait(Hooks.driver,100).until(ExpectedConditions.presenceOfElementLocated(By.xpath(strValue)));
				//((JavascriptExecutor) Hooks.driver).executeScript("arguments[0].scrollIntoView(true);", element);
				
				break;
				
			case "id":
				element =Hooks.driver.findElement(By.id(strValue));
				break;
			case "name":
				element =Hooks.driver.findElement(By.name(strValue));
				break;
			case "default":
				element=null;
		}
		
		return element;
	}
	
	public static void performValidation(String strObjct,String data) throws IOException, InterruptedException
	{
		WebElement we = UtilityFunctions.retunWebElement(strObjct);
		String tagName = we.getTagName();
		switch(tagName.toLowerCase())
		{
			case "strong":
				Assert.assertEquals(data,we.getText());	
				break;				
			case "code":
				Assert.assertTrue(we.getText().contains(data));
				System.out.println(we.getText());
				break;
			case "h1":
				Assert.assertEquals(data,we.getText());	
				break;
			case "button":
				boolean b=Boolean.parseBoolean(data); ;
				Assert.assertEquals(b,we.isEnabled());	
				break;	
			case "ul":
				Assert.assertEquals(data,we.getText());	
				break;
		}
		
		
	}
	//*********************************************************************
	//Create an element and then perform the respective action
	//*********************************************************************
	
	public static void performAction(String strObjct,String data) throws IOException, InterruptedException
	{
		WebElement we = UtilityFunctions.retunWebElement(strObjct);
		String tagName = we.getTagName();
		switch(tagName.toLowerCase())
		{
			case "button":
				we.click();		
				break;
			case "a":
				we.click();	
				break;
			case "span":
				we.click();	
				break;
			case "li":
				we.click();	
				break;
			case "label":
				we.click();	
				break;
			case "input":
				if(data.equals(""))				
				{
					we.click();	
					break;
				}
				else
				{
					we.clear();
				    we.sendKeys(data);
					break;
				}				
			case "textarea":
				we.clear();
				we.sendKeys(data);
				break;
			case "select":
				Select se=new Select(we);				
				se.selectByVisibleText(data);
				break;
				
		}
		
		
	}


	public static void waitForPageToBeReady() throws InterruptedException 
	{
		WebDriver driver =Hooks.driver;
	    JavascriptExecutor js = (JavascriptExecutor)driver;

	    //This loop will rotate for 100 times to check If page Is ready after every 1 second.
	    //You can replace your if you wants to Increase or decrease wait time.
	    for (int i=0; i<400; i++)
	    { 
	       	Thread.sleep(6000);	       
            //To check page ready state.
	        if (js.executeScript("return document.readyState").toString().equals("complete"))
	        { 
	            break; 
	        }   
	      }
	 }
	
	public static void scrollDownWindow() throws InterruptedException 
	{
	WebDriver driver =Hooks.driver;
	JavascriptExecutor js = (JavascriptExecutor)driver;
    js.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight," +
    "document.body.scrollHeight,document.documentElement.clientHeight));");
	}
	//*********************************************************************
	//Switch between browser tabs as per the given tab index
	//*********************************************************************
		
	public static void switchBetweenTabs(int tabNum) throws InterruptedException
	{
		Thread.sleep(5000);
		ArrayList<String> tabs2 = new ArrayList<String> (Hooks.driver.getWindowHandles());
	    Hooks.driver.switchTo().window(tabs2.get(tabNum));
	    Thread.sleep(5000);
	    waitForPageToBeReady();
	    
	}
	
	public static void switchTabs() throws InterruptedException
	{
		parentWindow = Hooks.driver.getWindowHandle();
		System.out.println(parentWindow);
		Set<String> handles =  Hooks.driver.getWindowHandles();
		   for(String windowHandle  : handles)
		       {
		       if(!windowHandle.equals(parentWindow))
		          {
		    	   System.out.println(windowHandle); 
		    	   Hooks.driver.switchTo().window(windowHandle);
		       	        
		          }
		       }
	}

	
	
	
	
}
	