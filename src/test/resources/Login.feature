Feature: Verify the login functionality

@VerifyCreditCardPayment
 Scenario Outline: Successful Login with Valid Credentials
   Given Launch Payment center application
   When I set userName to itqa@sage.com
   And I set password to Sage1234
   And I click on Sign In
   And I click on continue
   Given In payment center Portal click on Accept Payments option.
    And I choose credit card option
    And I set cardNumber to <CardNumber>
    And I set subtotal to <Subtotal>
    And I click on HideIcon
    And I set firstName to <FirstName>
    And I set lastName to <LastName>
    When I click on Submit
    Then response message should be 'Payment Submitted'

Examples:
    | name         | password| CardName   | CardNumber       | ExpirationMonth | ExpirationYearIndex | CVV |FirstName|LastName|Subtotal|
    |itqa@sage.com |Sage1234  |Visa       | 4111111111111111 |               4 |                   2 | 123 |test     |test    |70      |
    |itqa@sage.com |Sage1234  |Mastercard | 5499740000000057 |               4 |                   2 | 123 |test     |test    |50      |
    |itqa@sage.com |Sage1234  |Discover   | 6011000993026909 |               4 |                   2 | 123 |test     |test    |30      |
    |itqa@sage.com |Sage1234  |Amex       | 37144963539237   |               4 |                   2 | 123 |test     |test    |50      |
    
    
 @VerifyVertualCheckPayment
 Scenario Outline: Successful Login with Valid Credentials
   Given Launch Payment center application
   When I set userName to itqa@sage.com
   And I set password to Sage1234
   And I click on Sign In
   And I click on continue
   Given In payment center Portal click on Accept Payments option.
   And I choose virtualCheck option
   And I set subtotal to <Subtotal>
   And I set routingNum to <RoutingNumber>
   And I set accountnum to <AccountNum>
   And I set firstName to <FirstName>
   And I set lastName to <LastName>
   And I set address to <Address>
   And I set city to <City>
   And I set zipcode to <Zipcode>
   When I click on Submit 
    
    
    Examples:
  |Address   | RoutingNumber |FirstName |LastName |Subtotal|City     |Zipcode|AccountNum |
  |Visa      | 123123123     |test1     |test1    |70      |Bangalore|12345  |12345678904|
  |Mastercard| 123123123     |test2     |test2    |50      |sss      |23456  |90876543210|
  |Discover  | 123123123     |test3     |test3    |30      |fgfg     |78965  |56789034567|
  |Amex      | 123123123     |test4     |test4    |60      |sdsfd    |51290  |67890234567|
    
    
    
    
    
    